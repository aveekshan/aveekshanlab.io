import styles from "./Name.module.css";

export default function Name() {
    return (
        <div className={styles['name-wrapper']}>
            <p className={styles['name']}>Veekshan Allada</p>
        </div>
    );
}
