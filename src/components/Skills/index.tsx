import styles from "./Skills.module.css";

export default function Skills({ list }: any) {
    return (
        <div className={styles.skills}>
            {list.map((item: any) => (
                <div key={item}>
                    <p>{item}</p>
                </div>
            ))}
        </div>
    );
}
