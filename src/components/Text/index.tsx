import styles from "./Text.module.css";

export default function Text({ text, desc }: any) {
    return (
        <p className={`${styles.text} ${desc ? styles["text-desc"] : ""}`}>
            {text}
        </p>
    );
}
