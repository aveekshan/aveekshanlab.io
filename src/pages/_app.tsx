import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { Noto_Sans } from "next/font/google";

const inter = Noto_Sans({
    weight: ["600", "100", "200", "300", "400", "500", "700", "800", "900"],
    style: ["normal", "italic"],
    subsets: [
        "cyrillic",
        "cyrillic-ext",
        "devanagari",
        "greek",
        "greek-ext",
        "latin",
        "latin-ext",
        "vietnamese",
    ],
    display: "swap",
});

export default function App({ Component, pageProps }: AppProps) {
    return (
        <main className={inter.className}>
            <Component {...pageProps} />
        </main>
    );
}
