import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="en">
      <Head >
        <meta name="theme-color" content="#000000" />
        <meta name="description" content="UI Engineer" />
        <meta name="keywords" content="Veekshan Allada ,Veekshan, Veekshan UX/UI Engineer" />
        <link rel="apple-touch-icon" href="/logo192.png" />

        <meta property="og:title" content="Veekshan" />
        <meta property="og:url" content="https://aveekshan.gitlab.io/" />
        <meta property="og:image" content="https://aveekshan.gitlab.io/" />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
