import Head from "next/head";
import styles from "@/styles/Home.module.css";
import Profile from "@/components/Profile";
import Name from "@/components/Name";
import Text from "@/components/Text";
import Skills from "@/components/Skills";

export default function Home(props: any) {

    return (
        <>
            <Head>
                <title>Veekshan</title>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main className={styles.wrapper}>
                <div className={styles.intro}>
                    <div style={{ flexBasis: "50%" }}>
                        <Text text="Hello, My name is" />
                        <Name />
                        <Text desc text={props?.data[0]?.info} />
                        <Skills list={props?.data[2]?.list} />
                    </div>
                    <Profile />
                </div>
            </main>
        </>
    );
}

export const getStaticProps = async () => {
    return {
        props: {
            ...require(`@/const/info.json`),
        },
    };
};
